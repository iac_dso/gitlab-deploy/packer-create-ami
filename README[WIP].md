#### Deployment Notes

<u>**Additional files & Directories:**<br></u>

  > Before deploying you'll need to add the following files and directories.<br/>
  > All paths are relative to the project directory


  <u>**Certificates:**<br></u>
  - **Directory**: `ansible/gitlab/files`
  - **file**: `rds-combined-ca-gov-bundle.pem`
  - **file**: `gc-adte-certchain.pem`

The pem files above can found in: `s3://{gc|gd}-adte-certs/WindowsRoot`.

<u>**Config settings:**<br></u>
  - **ssh_password & become_password:**<br>
    - Since Packer doesn't support retrieval of information from the parameter store, I can't include them in the project dir. They can be found in the **Systems Manager parameter store**.
  - **AWS_Profile:** Be sure to set this value to match your local AWS configs
  - **Release:** Be sure to increment this value as **the build will not complete if a duplicate AMI is found.**

---
<u>**Deploying the Image:**<br></u>
  -  Run **`packer build packer`** from the project directory.
  -  Packer version 1.5.5 or above is required to deploy the AMI.
