//This file contains all of the input variables for the Gitlab AMI

variable "env" {
  description = "Abbreviated name of environment that the resource will be deployed to. eg: gc, gt, gd"
  type        = string
}

variable "stage" {
  description = "Current development stage. eg: Dev, Staging, Prod"
  type        = string
  default     = "Dev"
}

variable "env_vpc" {
  description = "abbreviated name of VPC that the resource will be deployed to. eg: gc, gt, gd"
  type        = string
  default     = "vdms"
}

variable "application_name" {
  description = "Name of the application that you will be deploying"
  type        = string
  default     = "gitlab"
 }

variable "application_version" {
   type = string
   description = "Version number of the main application."
 }

variable "release" {
  description = "Version number of the resulting AMI."
  type        = string
 }

variable "source_ami" {
    type = string
 }

variable "team" {
  description = "Name of the team that is responsible for maintaining the AMI."
  type = string
  default = "DevSecOps"
}

variable "subnet_role" {
  description = "Value of the `Role` tag of the subnet the packer instance will be deployed to."
  type = string
  default = "mgmt"
 }

variable "owner" {
    type = string
    default = "tbradley"
 }

variable "ssh_keypair_name" {
    type = string
    default = "svc-packer"
 }

variable "ssh_private_key_file" {
    type = string
    default = "ansible-svc-id-rsa"
 }

variable "role" {
    type = string
    default = "CI-CD-Orchestrator"
 }

variable "project" {
    type = string
    default = "EAS"
 }

variable "platform" {
    type = string
    default = "RHEL-7.7"
 }

variable "ssh_username" {
    type = string
    default ="svc-ansible"
 }

variable "ssh_password" {
    type = string
 }

variable "become_password" {
    type = string
 }
