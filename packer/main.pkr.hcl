
source "amazon-ebs" "this" {
  ssh_username = "${var.ssh_username}"
  ssh_password = "${var.ssh_password}"
  instance_type = "t3.small"
  encrypt_boot = true
  ami_description = "${var.application_name}"
  ami_name = "${var.application_name}-${var.release}"
  force_delete_snapshot = true
  source_ami = "${var.source_ami}"
  shutdown_behavior = "terminate"
  decode_authorization_messages = true

  vpc_filter {
  // find VPC using the `DetailEnv` tag
    filter {
      name = "tag:DetailEnv"
      value = "${var.env}-${var.env_vpc}"
    }
  }

  subnet_filter {
  //find subnet using the `Role` tag
    filter {
        name = "tag:Role"
        value = "${var.subnet_role}"
    }
    most_free = "true"
  }

  dynamic "run_tag" {
  //tags for Packer instance
     for_each = local.tags
     iterator = tag
     content {
       name = tag.key
       value = tag.value
     }
   }


  dynamic "tag" {
  //tags for AMI
   for_each = local.tags
   iterator = tag
   content {
     name = tag.key
     value = tag.value
   }
  }

  run_volume_tags {
    // tags for the packer instance volumes
    // I tried to make this block dynamic like the other tag blocks, but
    // it isn't supported right now... at least it makes the file shorter
    Environment        = "${var.stage}"
    Name               = "ami-${var.application_name}-${var.release}"
    Application        = "${var.application_name}"
    ApplicationVersion = "${var.application_version}"
    Owner              = "${var.owner}"
    Project            = "${var.project}"
    team               = "${var.team}"
    Role               = "${var.role}"
    Provisioner        = "Packer"
    Release            = "${var.release}"
    Platform           = "${var.platform}"
  }
}

build {
  sources = ["source.amazon-ebs.this"]


  provisioner "ansible" {
        extra_arguments = [
           "-e ansible_become_password=${var.become_password}",
           "-e gitlab_ver=${var.application_version}"
        ]
        ansible_env_vars = []
        playbook_file = "ansible/provision-img.yml"
  }

}
