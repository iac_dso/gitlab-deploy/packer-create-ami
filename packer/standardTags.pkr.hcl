locals {
  tags = {
    Environment        = "${var.stage}"
    Name               = "ami-${var.application_name}-${var.release}"
    Application        = "${var.application_name}"
    ApplicationVersion = "${var.application_version}"
    Owner              = "${var.owner}"
    Project            = "${var.project}"
    team               = "${var.team}"
    Role               = "${var.role}"
    Provisioner        = "Packer"
    Release            = "${var.release}"
    Platform           = "${var.platform}"
  }
}
